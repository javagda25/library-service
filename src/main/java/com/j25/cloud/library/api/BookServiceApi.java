package com.j25.cloud.library.api;

import com.j25.cloud.library.model.Book;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient("book-service")
public interface BookServiceApi {
    @GetMapping("/{id}")
    public Book getBookById(@PathVariable("id") Long id);

    @PostMapping()
    public Book add(@RequestBody Book book);

    @GetMapping("/")
    public List<Book> find(@RequestParam(name = "title", required = false) String title,
                           @RequestParam(name = "author", required = false) String author);
}
