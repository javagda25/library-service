package com.j25.cloud.library.service;

import com.j25.cloud.library.api.BookServiceApi;
import com.j25.cloud.library.model.Book;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class BookApiCallsService {
    private final BookServiceApi bookServiceApi;

    public void addBook(Book book){
        bookServiceApi.add(book);
    }

    public List<Book> getAll(String author, String title) {
        return bookServiceApi.find(title, author);
    }
}

