package com.j25.cloud.library.controller;

import com.j25.cloud.library.model.Book;
import com.j25.cloud.library.service.BookApiCallsService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
@AllArgsConstructor
public class BookController {
    private final BookApiCallsService bookApiCallsService;

    @GetMapping("/add")
    public String add(){
        return "book-form";
    }

    @GetMapping("/search")
    public String searchForm(){
        return "book-search";
    }

    @PostMapping("/search")
    public String search(Model model, String author, String title){
        List<Book> books = bookApiCallsService.getAll(author, title);
        model.addAttribute("books", books);
        return "book-list";
    }

    @PostMapping("/add")
    public String add(Book book){
        bookApiCallsService.addBook(book);
        return "redirect:/add";
    }
}
